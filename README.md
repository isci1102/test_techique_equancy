# Test techique Equancy


Dans ce repository vous trouverez le notebook contenant les differentes analyses menées sur des profils professionels de candidats à des postes dans la data.

Sommaire du notebook : 

       1.Analyse exploratoire des données
       2.Clustering : Identification de groupes de profils
       3.Prediction du metier d'un candidat
